#
CC	=cc
CFLAGS	=-O2 -pipe  -fstack-protector-strong -fno-strict-aliasing 
#
#	Where to put the binary
#
BIN	=/usr/local/bin
#
MAINV	= 1.1
SUBV	= 14
VERSION	= $(MAINV).$(SUBV)
FILES	= README README.md Makefile tclCheck.c COPYRIGHT LICENSE tclCheck.1
#
all	: tclCheck

tclCheck : tclCheck.o
	$(CC) -s -o tclCheck tclCheck.o $(LIBS)
	
install	: tclCheck
	install -s tclCheck $(BIN)

clean	:
	rm -f tclCheck tclCheck.o
